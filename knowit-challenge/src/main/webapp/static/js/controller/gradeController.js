'use strict';

function naitaVormi() {
	$("#lisaVorm").toggle(400);
}

App.controller('GradeController',
		[
				'$scope',
				'GradeService',
				function($scope, GradeService) {

					var self = this;

					self.studentGrades;
					self.studentGrade = {
						id : null,
						grade : '',
						weight : '',
						subject : {
							id : null,
							name : ''
						},
						student : {
							id : null,
							firstName : '',
							lastName : ''
						}
					};
					self.avgStudent;
					self.subjects;

					self.getStudentInfo = function() {
						GradeService.getStudentInfo().then(function(d) {
							self.avgStudent = d;
						})
					};

					self.getStudentGrades = function(id, firstName, lastName) {
						sessionStorage.setItem("firstName", firstName);
						sessionStorage.setItem("lastName", lastName);
						self.firstName = sessionStorage.getItem("firstName")
						self.lastName = sessionStorage.getItem("lastName")
						sessionStorage.setItem("studentId", id);
						GradeService.findStudentGrades(
								sessionStorage.getItem("studentId")).then(
								function(d) {
									self.studentGrades = d;
								});
					};

					self.getStudentGrade = function() {
						GradeService.findStudentGrades(
								sessionStorage.getItem("studentId")).then(
								function(d) {
									self.studentGrades = d;
								});
					};

					if (sessionStorage.getItem("studentId") != null) {
						self.getStudentGrades(sessionStorage
								.getItem("studentId"), sessionStorage
								.getItem("firstName"), sessionStorage
								.getItem("lastName"))
					}
					self.getStudentInfo();

					self.updateStudentGrade = function(studentGrade, id) {
						GradeService.updateStudentGrade(studentGrade, id).then(
								self.getStudentGrade);
					};

					self.createStudentGrade = function(studentGrade) {

						GradeService.createStudentGrade(studentGrade).then(
								self.getStudentGrade);
					};

					self.muuda = function(id) {
						GradeService.getSubjects().then(function(d) {
							self.subjects = d;
						})
						for (var i = 0; i < self.studentGrades.length; i++) {
							if (self.studentGrades[i].id === id) {
								self.studentGrade = angular
										.copy(self.studentGrades[i]);
								break;
							}
						}
						$("#lisaVorm").toggle(400);
					};

					self.submit = function() {
						if (self.studentGrade.id === null) {
							self.createStudentGrade(self.studentGrade);
						} else {
							self.updateStudentGrade(self.studentGrade,
									self.studentGrade.id);
						}
						$("#lisaVorm").hide(400)
						self.reset()
					};

					self.reset = function() {
						GradeService.getSubjects().then(function(d) {
							self.subjects = d;
						})
						self.studentGrade = {
							id : null,
							grade : '',
							weight : '',
							subject : {
								id : null,
								name : ''
							},
							student : {
								id : sessionStorage.getItem("studentId")
							}
						};
					};

					self.deleteStudentGrade = function(id) {
						GradeService.deleteStudentGrade(id).then(
								self.getStudentGrade);
					};

					self.kustuta = function(id) {
						if (self.studentGrade.id === id) {
							self.reset();
						}
						self.deleteStudentGrade(id);
					};

				} ]);