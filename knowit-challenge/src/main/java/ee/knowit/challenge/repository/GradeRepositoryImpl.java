package ee.knowit.challenge.repository;

import ee.knowit.challenge.entity.GradeEntity;
import ee.knowit.challenge.entity.StudentAvg;
import ee.knowit.challenge.entity.StudentEntity;
import ee.knowit.challenge.entity.SubjectEntity;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository

public class GradeRepositoryImpl implements GradeRepository {

	@PersistenceContext
	EntityManager entityManager;

	protected Session getCurrentSession() {
		return entityManager.unwrap(Session.class);
	}

	@Transactional
	public void create(GradeEntity gradeEntity) {
		entityManager.persist(gradeEntity);
	}

	@Override
	public List<StudentAvg> getGrades() {

		TypedQuery<GradeEntity> query = entityManager.createQuery("SELECT g FROM GradeEntity g", GradeEntity.class);

		List<GradeEntity> objList = query.getResultList();
		ArrayList<Integer> studentList = new ArrayList<Integer>();
		List<StudentAvg> nameAndAvg = new ArrayList<StudentAvg>();

		for (int i = 0; i < objList.size(); i++) {
			if (!studentList.contains(objList.get(i).getStudent().getId())) {
				studentList.add(objList.get(i).getStudent().getId());
			}
		}

		for (int i = 0; i < studentList.size(); i++) {
			StudentAvg studentAvg = new StudentAvg();
			double hinneKordaKaal = 0;
			double kaaludeSumma = 0;
			for (int j = 0; j < objList.size(); j++) {
				if (objList.get(j).getStudent().getId() == studentList.get(i)) {
					hinneKordaKaal += objList.get(j).getGrade() * objList.get(j).getWeight();
					kaaludeSumma += objList.get(j).getWeight();
					studentAvg.setId(studentList.get(i));
					studentAvg.setFirstName(objList.get(j).getStudent().getFirstName());
					studentAvg.setLastName(objList.get(j).getStudent().getLastName());
					studentAvg.setAvgGrade(Math.round((hinneKordaKaal / kaaludeSumma) * 100.0) / 100.0);
				}
			}
			nameAndAvg.add(studentAvg);
		}
		return nameAndAvg;
	}

	@Override
	public List<GradeEntity> getById(Integer id) {
		TypedQuery<GradeEntity> query = entityManager.createQuery("SELECT g FROM GradeEntity g WHERE student_id = :id",
				GradeEntity.class);
		query.setParameter("id", id);
		return query.getResultList();
	}

	@Override
	public List<StudentEntity> getStudents() {
		TypedQuery<StudentEntity> query = entityManager.createQuery("SELECT g FROM StudentEntity g",
				StudentEntity.class);
		return query.getResultList();
	}

	@Transactional
	public void deleteGrade(Integer id) {
		GradeEntity gradeEntity = entityManager.find(GradeEntity.class, id);
		entityManager.remove(gradeEntity);
	}

	@Transactional
	public void updateStudentGrade(GradeEntity gradeEntity) {
		entityManager.merge(gradeEntity);
	}

	@Override
	public List<SubjectEntity> getSubjects() {
		TypedQuery<SubjectEntity> query = entityManager.createQuery("SELECT g FROM SubjectEntity g",
				SubjectEntity.class);
		return query.getResultList();
	}

}