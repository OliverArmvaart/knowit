'use strict';

App.factory('GradeService', [
		'$http',
		'$q',
		function($http, $q) {

			var baseUrl = "http://localhost:8080/knowit/";

			return {

				getStudentInfo : function() {
					return $http.get(baseUrl + "/grades").then(
							function(response) {
								return response.data;
							});
				},
				
				getSubjects : function() {
					return $http.get(baseUrl + "/grades/subjects").then(
							function(response) {
								return response.data;
							});
				},

				findStudentGrades : function(id) {
					return $http.get(baseUrl + "/grades/" + id).then(
							function(response) {
								return response.data;
							});
				},

				createStudentGrade : function(studentGrade) {
					return $http.post(baseUrl + '/grades/create',
							angular.toJson(studentGrade)).then(
							function(response) {
								return response.data;
							});
				},

				updateStudentGrade : function(studentGrade, id) {
					return $http.put(baseUrl + '/grades/update/' + id,
							studentGrade).then(function(response) {
						return response.data;
					});
				},
				
				deleteStudentGrade : function(id){
					return $http.delete(baseUrl + '/grades/delete/'+id).then(function(response){
						return response.data;
					})
				}
				
			};
		} ]);