package ee.knowit.challenge.repository;

import ee.knowit.challenge.entity.GradeEntity;
import ee.knowit.challenge.entity.StudentAvg;
import ee.knowit.challenge.entity.StudentEntity;
import ee.knowit.challenge.entity.SubjectEntity;

import java.util.List;

public interface GradeRepository {

	public void create(GradeEntity gradeEntity);

	public List<StudentAvg> getGrades();

	public List<SubjectEntity> getSubjects();

	public void deleteGrade(Integer id);

	public List<GradeEntity> getById(Integer id);

	public List<StudentEntity> getStudents();

	public void updateStudentGrade(GradeEntity gradeEntity);

}
