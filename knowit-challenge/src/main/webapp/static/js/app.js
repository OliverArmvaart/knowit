var App = angular.module('myApp', ['ngMaterial', 'ngRoute']);

App.config(function($routeProvider) {	
	  $routeProvider
	  .when('/home', {
	    templateUrl : 'static/views/Home.html',
	    controller  : 'GradeController'
	  })

	  .when('/studentGrades', {
	    templateUrl : 'static/views/StudentGrade.html',
	    controller  : 'GradeController'
	  })
	  
	  .otherwise({redirectTo: '/home'});
});
	  
App.$inject = ['$sessionStorage'];
App.$inject = ['$locationProvider'];