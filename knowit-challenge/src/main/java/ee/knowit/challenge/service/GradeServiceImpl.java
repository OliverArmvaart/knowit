package ee.knowit.challenge.service;

import ee.knowit.challenge.entity.GradeEntity;
import ee.knowit.challenge.entity.StudentAvg;
import ee.knowit.challenge.entity.StudentEntity;
import ee.knowit.challenge.entity.SubjectEntity;
import ee.knowit.challenge.repository.GradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GradeServiceImpl implements GradeService {

	@Autowired
	GradeRepository gradeRepository;

	@Override
	public void create(GradeEntity gradeEntity) {
		gradeRepository.create(gradeEntity);
	}

	@Override
	public List<StudentAvg> getGrades() {
		return gradeRepository.getGrades();
	}

	@Override
	public List<GradeEntity> getById(Integer id) {
		return gradeRepository.getById(id);
	}

	@Override
	public List<StudentEntity> getStudents() {
		return gradeRepository.getStudents();
	}

	@Override
	public void deleteGrade(Integer id) {
		gradeRepository.deleteGrade(id);
	}

	@Override
	public void updateStudentGrade(GradeEntity gradeEntity) {
		gradeRepository.updateStudentGrade(gradeEntity);
	}

	@Override
	public List<SubjectEntity> getSubjects() {
		return gradeRepository.getSubjects();
	}

}