package ee.knowit.challenge.controller;

import ee.knowit.challenge.entity.GradeEntity;
import ee.knowit.challenge.entity.StudentAvg;
import ee.knowit.challenge.entity.StudentEntity;
import ee.knowit.challenge.entity.SubjectEntity;
import ee.knowit.challenge.service.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class GradeController {

	@Autowired
	GradeService gradeService;

	@RequestMapping(value = "/")
	public String index() {
		return "static/index.html";
	}

	@RequestMapping(value = "/grades", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<StudentAvg> grades() {

		return gradeService.getGrades();
	}

	@RequestMapping(value = "/grades/subjects", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<SubjectEntity> getSubjects() {

		return gradeService.getSubjects();
	}

	@RequestMapping(value = "/grades/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<GradeEntity> getStudent(@PathVariable("id") Integer id) {
		return gradeService.getById(id);
	}

	@RequestMapping(value = "/students", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<StudentEntity> students() {
		return gradeService.getStudents();
	}

	@RequestMapping(value = "/grades/update/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void updateGrade(@RequestBody GradeEntity gradeEntity, @PathVariable("id") Integer id) {
		gradeService.updateStudentGrade(gradeEntity);
	}

	@RequestMapping(value = "/grades/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void createGrade(@RequestBody GradeEntity gradeEntity) {
		System.out.println("CREATE GRADE");
		gradeService.create(gradeEntity);
	}

	@RequestMapping(value = "/grades/delete/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteGrade(@PathVariable("id") Integer id) {
		System.out.println("DELETE GRADE");
		gradeService.deleteGrade(id);
	}

}